package de.cherriz.master.service.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Starterklasse für einen Springbootservice.
 *
 * @author Frederik Kirsch
 */
@Configuration
@EnableJpaRepositories(basePackages = "de.cherriz.master.service.user")
@EnableAutoConfiguration
@ComponentScan
@PropertySource("classpath:application.properties")
public class Application extends SpringBootServletInitializer {

    /**
     * Startet den Service.
     *
     * @param args Startparameter.
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected final SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

}