package de.cherriz.master.service.user;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entitaet eines Users.
 *
 * @author Frederik Kirsch
 */
@Entity
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = User.USER_FINDBYNAME, query = "SELECT u from User u where u.vorname = :vorname and u.nachname = :nachname and u.type = 'Unique'"),
        @NamedQuery(name = User.USER_FINDBYADDRESS, query = "SELECT u from User u where u.strasse = :strasse and u.ort = :ort and u.plz = :plz and u.type = 'Unique'"),
        @NamedQuery(name = User.USER_FINDBYCONTACT, query = "SELECT u from User u where u.email = :mail and u.telefon = :telefon and u.type = 'Unique'")
})
public class User {

    /**
     * Named Query fuer das finden von Usern mit gleichem Namen.
     */
    public static final String USER_FINDBYNAME = "User.findByName";

    /**
     * Named Query fuer das finden von Usern mit gleicher Adresse.
     */
    public static final String USER_FINDBYADDRESS = "User.findByAddress";

    /**
     * Named Query fuer das finden von Usern mit selber Emailadresse und Telefonnummer.
     */
    public static final String USER_FINDBYCONTACT = "User.findByContact";

    /**
     * Userstatus ungeprueft.
     */
    public static final String TYPE_UNCHECKED = "Unchecked";

    /**
     * Userstatus eindeutig.
     */
    public static final String TYPE_UNIQUE = "Unique";

    /**
     * Userstatus Duplikat.
     */
    public static final String TYPE_DUPLICATE = "Duplicate";

    /**
     * Userstatus unklar.
     */
    public static final String TYPE_UNSURE = "Unsure";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String vorname;

    private String nachname;

    private String strasse;

    private Integer plz;

    private String ort;

    private String email;

    private String telefon;

    private String type;

    private Long vertreter;

    /**
     * Konstruktor.
     */
    public User() {
        this.type = TYPE_UNCHECKED;
    }

    /**
     * @return Die ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Setzt die ID.
     *
     * @param id Die ID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Der Vorname.
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Setzt den Vornamen.
     *
     * @param vorname Der Vorname.
     */
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    /**
     * @return Der Nachname.
     */
    public String getNachname() {
        return nachname;
    }

    /**
     * Setzt den Nachnamen.
     *
     * @param nachname Der Nachname.
     */
    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    /**
     * @return Die Strasse.
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * Setzt die Strasse.
     *
     * @param strasse Die Strasse.
     */
    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    /**
     * @return Die PLZ.
     */
    public Integer getPlz() {
        return plz;
    }

    /**
     * Setzt die PLZ.
     *
     * @param plz Die PLZ.
     */
    public void setPlz(Integer plz) {
        this.plz = plz;
    }

    /**
     * @return Der Ort.
     */
    public String getOrt() {
        return ort;
    }

    /**
     * Setzt den Ort.
     *
     * @param ort Der Ort.
     */
    public void setOrt(String ort) {
        this.ort = ort;
    }

    /**
     * @return Die Emailadresse.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setzt die Emailadresse.
     *
     * @param email Die Emailadresse.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return Die Telefonnummer.
     */
    public String getTelefon() {
        return telefon;
    }

    /**
     * Setzt die Telefonnummer.
     *
     * @param telefon Die Telefonnummer.
     */
    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    /**
     * @return Der Userstatus.
     */
    public String getType() {
        return type;
    }

    /**
     * Setzt den Userstatus.
     *
     * @param type Der Userstatus.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return Der Verteter.
     */
    public Long getVertreter() {
        return vertreter;
    }

    /**
     * Setzt den Vertreter.
     *
     * @param vertreter Der Vertreter.
     */
    public void setVertreter(Long vertreter) {
        this.vertreter = vertreter;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User) {
            if (((User) obj).getId().equals(this.getId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getClass().getName().hashCode() + this.getId().intValue();
    }

}