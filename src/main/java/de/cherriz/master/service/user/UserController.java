package de.cherriz.master.service.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.core.NoContentException;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Controller fuer den REST Service fuer Mails.
 *
 * @author Frederik Kirsch
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    /**
     * Setzt den Service für die Verarbeitung.
     *
     * @param userService Der Userservice.
     */
    @Inject
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    /**
     * Methode dient zur Pruefung der Verfuegbarkeit des Services.
     *
     * @return den Status des Services.
     */
    @RequestMapping(value = "/systemcheck", method = RequestMethod.GET)
    public String hello() {
        String result = "SYSTEMCHECK:";
        result += "EntityManager: " + (this.userService != null ? "OK" : "Error");
        return result;
    }

    /**
     * Liefert zu einer ID den zugehoerigen User.
     *
     * @param id Die UserID.
     * @return Der User.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User get(@PathVariable Long id) {
        User user = this.userService.find(id);
        return user;
    }

    /**
     * Speichert einen User. Korrigiert den Userstatus und liefert diesen zurueck.
     *
     * @param user Der zu speichernde User.
     * @return Der gespeicherte User.
     */
    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    public User create(@RequestBody @Valid User user) {

        String type = this.getType(user);
        user.setType(type);

        if (User.TYPE_UNSURE.equals(type) || User.TYPE_UNIQUE.equals(type)) {
            user = this.userService.create(user);
        }

        return user;
    }

    /**
     * Suchen von Usern mit gleichem Namen.
     *
     * @param nachname Der Vorname.
     * @param vorname  Der Nachname.
     * @return Die gefundenen User.
     */
    @RequestMapping(value = "/name/{nachname},{vorname}", method = RequestMethod.GET)
    public List<User> getByName(@PathVariable String nachname, @PathVariable String vorname) {
        return this.userService.findByName(nachname, vorname);
    }

    /**
     * Suchen von Usern mit gleicher Adresse.
     *
     * @param strasse Die Strasse.
     * @param ort     Der Ort.
     * @param plz     Die PLZ.
     * @return Die gefundenen User.
     */
    @RequestMapping(value = "/address/{strasse},{plz},{ort}", method = RequestMethod.GET)
    public List<User> getByAdress(@PathVariable String strasse, @PathVariable String ort, @PathVariable Integer plz) {
        return this.userService.findByAdress(strasse, ort, plz);
    }

    /**
     * Suchen von Usern mit selber Emailadresse und Telefonnummer.
     *
     * @param mail    Die Emailadresse.
     * @param telefon Die Telefonnummer.
     * @return Die gefundenen User.
     */
    @RequestMapping(value = "/contact/{mail},{telefon}", method = RequestMethod.GET)
    public List<User> getByContact(@PathVariable String mail, @PathVariable String telefon) {
        return this.userService.findByContact(mail, telefon);
    }

    /**
     * Aktualisiert den Status eines Users.
     *
     * @param id     Die UserID.
     * @param status Der Status.
     * @return Der aktualisierte User.
     * @throws NoContentException     Der User existiert nicht.
     * @throws NotAcceptableException Der Status ist ungueltig.
     */
    @RequestMapping(value = "/update/status/{id}", method = RequestMethod.POST)
    public User updateStatus(@PathVariable Long id, @RequestBody @Valid String status) throws NoContentException, NotAcceptableException {
        User user = this.get(id);

        if (user == null) {
            throw new NoContentException("Der User existiert nicht!");
        } else if (!User.TYPE_UNSURE.equals(user.getType())) {
            throw new NotAcceptableException("Der Status des Usesrs ist ungültig.");
        } else if (!User.TYPE_UNIQUE.equals(status) && !User.TYPE_DUPLICATE.equals(status)) {
            throw new NotAcceptableException("Ungültiger Zielstatuts");
        }

        if (User.TYPE_UNIQUE.equals(status)) {
            user.setType(User.TYPE_UNIQUE);
            this.userService.create(user);
        } else {
            this.userService.delete(user);
            user.setType(User.TYPE_DUPLICATE);
        }

        return user;
    }

    /**
     * Ordnet einem User einen Vertreter zu.
     *
     * @param id        Die UserID.
     * @param vertreter Die ID des Vertreters.
     * @return Der aktualisierte User.
     * @throws NoContentException Der User oder Vertreter existiert nicht.
     */
    @RequestMapping(value = "/update/vertreter/{id}", method = RequestMethod.POST)
    public User setVertreter(@PathVariable Long id, @RequestBody @Valid Long vertreter) throws NoContentException, NotAcceptableException {
        User user = this.get(id);

        if (user == null) {
            throw new NoContentException("Der User existiert nicht!");
        } else if (vertreter == null) {
            throw new NoContentException("Es ist kein Vertreter definiert.");
        }

        user.setVertreter(vertreter);
        this.userService.create(user);

        return user;
    }

    /**
     * Loescht den zu einer ID uebergebenen User.
     *
     * @param id Die UserID.
     * @return Der geloeshte User.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public User deleteUser(@PathVariable Long id) {
        User user = this.get(id);

        this.userService.delete(user);

        return user;
    }

    /**
     * Behandelt {@link javax.ws.rs.core.NoContentException} und uebersetzt sie in den entsprechenden HTTP Status.
     *
     * @param exception Die Exception.
     * @return Der HTTP Status.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public String handleNoContent(NoContentException exception) {
        return exception.getMessage();
    }

    /**
     * Behandelt {@link javax.ws.rs.NotAcceptableException} und uebersetzt sie in den entsprechenden HTTP Status.
     *
     * @param exception Die Exception.
     * @return Der HTTP Status.
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public String handleNotAcceptable(NotAcceptableException exception) {
        return exception.getMessage();
    }

    /**
     * Ermittelt den Stauts eines Users.
     * User bei denen entweder zu Name, Adresse oder Kontaktdaten bereits Eintraege in der Datenbank vorhanden sind erhalten den Status {@link User#TYPE_UNSURE}.
     * Stimmt alles ueberein so erhaelt er den Status {@link User#TYPE_DUPLICATE}.
     *
     * @param user Der User.
     * @return Der Status.
     */
    private String getType(User user) {
        String type = User.TYPE_UNIQUE;

        List<User> userByName = this.getByName(user.getNachname(), user.getVorname());

        List<User> userByAdress = this.getByAdress(user.getStrasse(), user.getOrt(), user.getPlz());

        List<User> userByContact = this.getByContact(user.getEmail(), user.getTelefon());

        LinkedHashSet<User> users = new LinkedHashSet<>();
        users.addAll(userByName);
        users.addAll(userByAdress);
        users.addAll(userByContact);

        if (users.size() > 0) {
            type = User.TYPE_UNSURE;
            for (User item : users) {
                if (user.getVorname().equals(item.getVorname())
                        && user.getNachname().equals(item.getNachname())
                        && user.getStrasse().equals(item.getStrasse())
                        && user.getOrt().equals(item.getOrt())
                        && user.getEmail().equals(item.getEmail())
                        && user.getTelefon().equals(item.getTelefon())) {
                    type = User.TYPE_DUPLICATE;
                    break;
                }
            }
        }

        return type;
    }

}