package de.cherriz.master.service.user;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Schnittstelle zum Repository fuer User.
 *
 * @author Frederik Kirsch
 */
public interface UserRepository extends JpaRepository<User, Long> {}