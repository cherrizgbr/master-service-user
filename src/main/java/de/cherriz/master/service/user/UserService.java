package de.cherriz.master.service.user;

import java.util.List;

/**
 * Schnittstelle fuer die Verarbeitung von Usern.
 *
 * @author Frederik Kirsch
 */
public interface UserService {

    /**
     * Loescht den uebergebenen User. Liefert anschließen den angepassten User zurueck.
     *
     * @param user Der User.
     * @return Der aktualisierte User.
     */
    User delete(User user);

    /**
     * Liefert einen User zu einer ID.
     *
     * @param id Die ID des Users.
     * @return Der User.
     */
    User find(Long id);

    /**
     * Suchen von Usern mit gleichem Namen.
     *
     * @param nachname Der Vorname.
     * @param vorname  Der Nachname.
     * @return Die gefundenen User.
     */
    List<User> findByName(String nachname, String vorname);

    /**
     * Suchen von Usern mit gleicher Adresse.
     *
     * @param strasse Die Strasse.
     * @param ort     Der Ort.
     * @param plz     Die PLZ.
     * @return Die gefundenen User.
     */
    List<User> findByAdress(String strasse, String ort, Integer plz);

    /**
     * Suchen von Usern mit selber Emailadresse und Telefonnummer.
     *
     * @param mail    Die Emailadresse.
     * @param telefon Die Telefonnummer.
     * @return Die gefundenen User.
     */
    List<User> findByContact(String mail, String telefon);

    /**
     * Speichert den uebergebenen User. Liefert anschließend den angepassten User zurueck.
     *
     * @param user Der User.
     * @return Der angepasste User.
     */
    User create(User user);

}