package de.cherriz.master.service.user;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Die Implementierung des {@link de.cherriz.master.service.user.UserService}.
 *
 * @author Frederik Kirsch
 */
@Service
@Validated
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @PersistenceContext
    private EntityManager em;

    /**
     * Setzt das Repository fuer den Datenbankzugriff.
     *
     * @param repository Das Repository.
     */
    @Inject
    public UserServiceImpl(final UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User create(User user) {
        this.repository.save(user);
        return user;
    }

    @Override
    public User delete(User user){
        this.repository.delete(user);
        return user;
    }

    @Override
    public User find(Long id) {
        return this.repository.findOne(id);
    }

    @Override
    public List<User> findByName(String nachname, String vorname) {
        return this.em.createNamedQuery(User.USER_FINDBYNAME, User.class).setParameter("vorname", vorname).setParameter("nachname", nachname).getResultList();
    }

    @Override
    public List<User> findByAdress(String strasse, String ort, Integer plz) {
        return this.em.createNamedQuery(User.USER_FINDBYADDRESS, User.class).setParameter("strasse", strasse).setParameter("ort", ort).setParameter("plz", plz).getResultList();
    }

    @Override
    public List<User> findByContact(String mail, String telefon) {
        return this.em.createNamedQuery(User.USER_FINDBYCONTACT, User.class).setParameter("mail", mail).setParameter("telefon", telefon).getResultList();
    }

}